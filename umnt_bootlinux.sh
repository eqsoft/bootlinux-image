#!/bin/bash

. ./environment

umount "${BOOTLINUX}"
echo "${BOOTLINUX} unmounted"
umount "${PERSISTLINUX}"
echo "${PERSISTLINUX} unmounted"
sleep 1
kpartx -dsv "${BOOTIMAGE}"


