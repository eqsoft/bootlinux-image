#!/bin/bash

docker run --privileged \
    -it \
    --rm \
    --name "bootlinux-image-cont" \
    --mount type=bind,source="$(pwd)",target=/build \
    -v /dev:/dev \
    -w "/build" \
    -e "BOOTLINUX_DOCKER=1" \
    registry.gitlab.com/eqsoft/bootlinux-image-docker:stretch \
    ./build-docker.sh
