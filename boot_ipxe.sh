#!/bin/bash

. ./environment

qemu-system-x86_64 \
	-enable-kvm \
	-m 2G \
	-net nic -net user,hostfwd=tcp::2222-:22 \
	-kernel ipxe.lkrn \
	-initrd seb3.ipxe

#qemu-system-x86_64 -enable-kvm -hda "${BOOTIMAGE}" -m 2G
