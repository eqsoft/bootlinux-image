#!/bin/bash
docker run \
    -d \
    --rm \
    --name "bootlinux-image-cont" \
    -p 8080:8080 \
    registry.gitlab.com/eqsoft/bootlinux-image-docker:stretch \
    /bin/bash -c 'supervisord'
