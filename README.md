bootlinux image creator for seb3
================================

## Introduction ##

* The main purpose of this project is the easy generation, customizing and testing of boot device images and netboot ressources for a bootlinux webkiosk system and an embedded seb3.
    * See also: https://gitlab.com/eqsoft/bootlinux
    * See also: https://gitlab.com/eqsoft/seb3

* The persistence partition is prepared for supporting e-exam offline caching in a boot device
    * see https://gitlab.com/eqsoft/ilias-offline-exam

## How it works ##

* The ```mkimage.sh``` script first creates a bootable device image with two partitions and an embedded syslinux bootloader.
* After that a pre-build filesystem.squashfs is fetched from bootlinux project, the size is optimized and the files are copied to the first partition.
* All ressources in ```persistence/*``` directory are copied to a read-writable partion 2 with volume label "persistence".
    * ```fs_overlay/*```: ressources are copied at boot-time into the root filesystem.
    * all other folder that are registered in ```persistance.conf``` are bind mounted into bootlinux and persists after the session.
        * see also: https://live-team.pages.debian.net/live-manual/html/live-manual/customizing-run-time-behaviours.en.html#548
* The ```../seb3/browser``` and ```../seb3/BROWSER_PROFILE``` are copied to ```persistence/data/```
* With ```boot.sh``` the created image is booted in a qemu-kvm virtual environment

## Build Requirements ##

* You need to be root user for building.

```
apt-get install kpartx syslinux syslinux-common qemu-system qemu-kvm squashfs-tools
```

* You need also a checked-out seb3 repo in the parent folder:
```
git clone https://gitlab.com/eqsoft/seb3.git ../seb3
```

* It is required to perform the seb3 setup and a proper installation of the chromium app, see https://gitlab.com/eqsoft/seb3
* The browser MUST be chromium in ../seb3/environment:
```
# SET DEFAULT BROWSER
BROWSER=$CHROMIUM
```

* Unfortunately the development workflow requires a fixed folder structur in Linux:
```
/data/seb3
/data/bootlinux-image
```
Maybe symlinks are your friends...

* It is recommanded to run a sebserver instance for the bootlinux backend (should be automatically started after seb3 setup)

## Customizing and Configuration ##

* First of all take a look at ```environment``` and adapt partition sizes to your needs:
```
# only MB!
# default filesystem.squashfs ca. 400MB
BOOTSIZE=700
# this depends on browser and custom profile size
PERSISTSIZE=1000
```

* Keep all other environment params. 
* ```mkimage.sh``` will always fetch a fresh prebuild filesystem.squashfs from 
```
FS_SOURCE="filesystem.squashfs"
# fetch latest artifact from gitlab.com
BOOTLINUX_SOURCE="https://gitlab.com/eqsoft/bootlinux/-/jobs/artifacts/stretch/raw/binary/live/${FS_SOURCE}?job=build" 
```
and then cache it to ./bootlinux.squashfs. 

* For subsequent builts you can use the cached bootlinux.squashfs by editing ```mkimage.sh```:
```
USE_FS_TEMPLATE=1
```

* After successful building the content of ```bootlinux.img``` can be easily customized:
```
./mnt_bootlinux.sh

``` 
edit files in:

```
/mnt/bootlinux/*
/mnt/persistlinux/*
```

```
./umnt_bootlinux.sh
./boot.sh
```

## Customizing the root filesystem instead of persistence layer ##

instead of customizing the fs_overlay or persistence mounts it is also possible to edit and embedd files directly in the root filesystem:

```
./mnt_bootlinux.sh
cp /mnt/bootlinux/live/bootlinux.squashfs /tmp
unsquashfs bootlinux.squashfs
-- EDIT FILES IN squashfs-root/* --
rm bootlinux.squashfs
mksquashfs squashfs-root bootlinux.squashfs -comp xz
mv bootlinux.squashfs /mn/bootlinux/live/
cd --TO_PROJECT_PATH--
./umnt-bootlinux.sh
./boot.sh
```

## Example for pubkey ssh configuration ##

```
./mnt_bootlinux.sh
cat ~/.ssh/id_rsa.pub >> /mnt/persistlinux/fs_overlay/etc/ssh/authorized_keys
./umnt_bootlinux.sh
./boot.sh
```

type in a new console:

```
ssh localhost -p 2222
```

## Netboot Configuration ##

* the bootlinux.img ressources can also be adapted and used in a PXE netboot environment:
```
./mnt_bootlinux.sh
```

take a look at:

* /mnt/bootlinux/live/*
* /mnt/bootlinux/boot/*

