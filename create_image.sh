#!/bin/bash

# script for generating a minimal linux hd image

. ./environment

img_confirm=""

. ./umnt_bootlinux.sh

if [ ! -f "${MBR}" ] ; then
	echo "syslinux mbr.bin does not exist."
	echo "install syslinux with 'apt-get install syslinux syslinux-common' and try again"
	exit 1
fi

if [ -f "${BOOTIMAGE}" ] ; then
    read -p  "WARNING: existing ${BOOTIMAGE} will be overridden: " -i 'y' -e img_confirm
    if [ "${img_confirm}" != "y" ] ; then
        echo "aborting..."
        exit 1
    fi
    rm "${BOOTIMAGE}"
fi

IMAGESIZE=$(($BOOTSIZE + $PERSISTSIZE))
echo "Image size to create: $IMAGESIZE MB ..."

# ratio of MByte settings in percent
P_BOOTSIZE=$(($(($BOOTSIZE * 100)) / $IMAGESIZE))
P_PERSISTSIZE=$(($(($PERSISTSIZE * 100)) / $IMAGESIZE))

dd if=/dev/zero of="${BOOTIMAGE}" bs=1M count="${IMAGESIZE}"
losetup /dev/loop0 $BOOTIMAGE
dd if="${MBR}" of=/dev/loop0

RAWSECTORS="$(sfdisk -l ${BOOTIMAGE} | head -n 1 | cut -d' ' -f7)"
SECTORS=$(($RAWSECTORS - $MBR_OFFSET_SECTORS))

BOOT_SECTORS=$(($(($P_BOOTSIZE * $SECTORS)) / 100))
PERSIST_SECTORS=$(($(($P_PERSISTSIZE * $SECTORS)) / 100))

SUM_SECTORS=$(($BOOT_SECTORS + $PERSIST_SECTORS))

if [ "$SUM_SECTORS" -lt "$SECTORS" ] ; then
    PERSIST_SECTORS=$(($PERSIST_SECTORS + $(($SECTORS - $SUM_SECTORS))))
fi
if [ "$SUM_SECTORS" -gt "$SECTORS" ] ; then
    PERSIST_SECTORS=$(($PERSIST_SECTORS - $(($SUM_SECTORS - $SECTORS))))
fi

SUM_SECTORS=$(($BOOT_SECTORS + $PERSIST_SECTORS))

if [ "$SUM_SECTORS" -ne "$SECTORS" ] ; then
    echo "something wrong with sectors calculation of partition..."
    echo "available sectors: $SECTORS"
    echo "calculated sectors: $SUM_SECTORS"
    exit 1
fi

export BOOT_START=$MBR_OFFSET_SECTORS
export BOOT_SECTORS=$BOOT_SECTORS
export PERSIST_START=$(($MBR_OFFSET_SECTORS + $BOOT_SECTORS))
export PERSIST_SECTORS=$PERSIST_SECTORS

envsubst \$BOOT_START,\$BOOT_SECTORS,\$PERSIST_START,\$PERSIST_SECTORS < $TPL_BOOTIMAGE_SFDISK > $BOOTIMAGE_SFDISK

# with partitiondata
sfdisk /dev/loop0 < $BOOTIMAGE_SFDISK

# detach /dev/loop0
losetup -d /dev/loop0

# re-attach with dev mapper tool kpartx and create filesystems
while read -r loop; do
	if [[ $loop =~ p1 ]] ; then
        mkfs.vfat -F 32 "/dev/mapper/${loop}"
        syslinux -i "/dev/mapper/${loop}"
	fi
	if [[ $loop =~ p2 ]] ; then
        mkfs.ext4 "/dev/mapper/${loop}" -L "${PERSISTLABEL}"
    fi
done <<< "$(kpartx -asv ${BOOTIMAGE} | cut -d' ' -f3)"

# detach again
kpartx -dvs $BOOTIMAGE

exit 0
