#!/bin/bash

. ./environment

if [ -d "${BOOTLINUX}/boot" ] ; then
	. ./umnt_bootlinux.sh
fi
#kvm -hdb "${BOOTIMAGE}" -m 2G
qemu-system-x86_64 \
	-enable-kvm \
	-boot order=c,menu=on \
	-drive file="${BOOTIMAGE}",index=0,media=disk,format=raw \
	-m 2G \
	-net nic -net user,hostfwd=tcp::2222-:22
#qemu-system-x86_64 -enable-kvm -hda "${BOOTIMAGE}" -m 2G
