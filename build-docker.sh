#!/bin/bash

# script for generating a minimal linux hd image

# check if executed in docker container
# see export BOOTLINUX_DOCKER=1 in build.sh
if [ -z "${BOOTLINUX_DOCKER}" ] ; then
    echo "I need to be run in the docker container!"
    echo "Please execute ./build.sh to start me from inside the docker container."
    exit 1
fi

. ./environment

CWD=$(pwd)

./create_image.sh

if [ $? -ne 0 ] ; then
    exit 1
fi

# now mount the image
. ./mnt_bootlinux.sh

# alter and copy the bootlinux filesystem from original source into bootlinux.squashfs for the image

echo "change to /tmp folder" 
cd /tmp
if [ -f "${FS_BOOTLINUX}" ] ; then
    rm "${FS_BOOTLINUX}"
fi
echo "try to get ${FS_BOOTLINUX} ..."
if [ -f "${CWD}/${FS_BOOTLINUX}" ] ; then
	read -p "Use existing cached image: " -i 'y' -e confirm
    if [ "${confirm}" != "y" ] ; then
        wget -O "${FS_BOOTLINUX}" "${BOOTLINUX_SOURCE}"
    else
        cp "${CWD}/${FS_BOOTLINUX}" .
    fi
else 
    wget -O "${FS_BOOTLINUX}" "${BOOTLINUX_SOURCE}"
fi

if [ ! -f  "${FS_BOOTLINUX}" ] ; then
    echo "could not fetch ${BOOTLINUX_SOURCE}"
    exit 1
fi
if [ -d "${FS_BOOTLINUX_ROOT}" ] ; then
    echo "remove existing ${FS_BOOTLINUX_ROOT} ..."
    rm -r "${FS_BOOTLINUX_ROOT}"
fi
echo "unsquashfs ${FS_BOOTLINUX}..."
unsquashfs -d "${FS_BOOTLINUX_ROOT}" "${FS_BOOTLINUX}"
if [ ! -d  "${FS_BOOTLINUX_ROOT}" ] ; then
    echo "unsquashfs failed!"
    exit 1
fi
if [ ! -d "${BOOTLINUX}/live" ] ; then
    mkdir -p "${BOOTLINUX}/live"
fi

echo "cache ${FS_BOOTLINUX} to ${CWD}/${FS_BOOTLINUX}"
mv "${FS_BOOTLINUX}" "${CWD}/${FS_BOOTLINUX}"

echo "get default user..."
USERNAME=$(cat ${FS_BOOTLINUX_ROOT}/etc/default/nodm | grep NODM_USER | cut -d'=' -f2)
echo "USER:${USERNAME}"

echo "move boot ressources from unsquashed filesystem to ${BOOTLINUX} and ${CWD}/boot"

#echo "copy ${FS_BOOTLINUX_ROOT}/initrd.img" "${BOOTLINUX}/live/initrd.img"
cp -L "${FS_BOOTLINUX_ROOT}/initrd.img" "${BOOTLINUX}/live/initrd.img"
#echo "copy ${FS_BOOTLINUX_ROOT}/vmlinuz" "${BOOTLINUX}/live/vmlinuz"
cp -L "${FS_BOOTLINUX_ROOT}/vmlinuz" "${BOOTLINUX}/live/vmlinuz"
#echo "copy ${FS_BOOTLINUX_ROOT}/initrd.img" "${CWD}/boot"
cp -L "${FS_BOOTLINUX_ROOT}/initrd.img" "${CWD}/boot/live"
#echo "copy ${FS_BOOTLINUX_ROOT}/vmlinuz" "${CWD}/boot"
cp -L "${FS_BOOTLINUX_ROOT}/vmlinuz" "${CWD}/boot/live"

echo "delete boot ressources from ./${FS_BOOTLINUX_ROOT}" 
find ./"${FS_BOOTLINUX_ROOT}" -iname vmlinuz* -delete
find ./"${FS_BOOTLINUX_ROOT}" -iname initrd.img* -delete
find ./"${FS_BOOTLINUX_ROOT}/boot" -name vmlinuz* -delete
find ./"${FS_BOOTLINUX_ROOT}/boot" -name initrd.img* -delete

# embed browser

cd /opt/seb3
. ./environment

# override installation environment
# install in user skeleton
DEVMODE=0
INSTALL_PATH="/tmp/${FS_BOOTLINUX_ROOT}/${SEB3_SKEL_PATH}"

mkdir -p "${INSTALL_PATH}"

# runtime path is the home directory, the path is only for the json registry and does not exist at boot time!
NATIVE_MESSAGING_INSTALL_PATH="/home/${USERNAME}/.config/seb3/${BROWSER}-userdata/NativeMessagingHosts"

. ./install-seb3.sh

cd "${CWD}"

# prepare boot and persistence
# copy syslinux bios modules
mkdir -p "${BOOTLINUX}/boot/syslinux"
cp "${BIOS}"/* "${BOOTLINUX}"/boot/syslinux/

export USERNAME="${USERNAME}"
export FS_BOOTLINUX="${FS_BOOTLINUX}"

# overwrite custom syslinux.cfg
envsubst \$USERNAME,\$FS_BOOTLINUX < tpl.syslinux.cfg > "${BOOTLINUX}/boot/syslinux/syslinux.cfg"

# copy data to persistence partition
cp -a persistence/* "${PERSISTLINUX}/"

cd /tmp

mksquashfs "${FS_BOOTLINUX_ROOT}" "${FS_BOOTLINUX}" -comp xz
rm -r "${FS_BOOTLINUX_ROOT}"

# move filesystem and other ressources
cp "${FS_BOOTLINUX}" "${BOOTLINUX}/live"
mv "${FS_BOOTLINUX}" "${CWD}/boot/live"

# cp "${CWD}/ipxe.lkrn" "${CWD}/boot/ipxe"
# cp "${CWD}/ipxe.lkrn" "${BOOTLINUX}/live"
# cp "${CWD}/seb3.ipxe" "${CWD}/boot/ipxe"
# cp "${CWD}/seb3.ipxe" "${BOOTLINUX}/live"

# change to bootlinux directory
cd $CWD

. ./umnt_bootlinux.sh

exit 0
