#!/bin/bash

. ./environment

if [ ! -d "${BOOTLINUX}" ] ; then
	mkdir "${BOOTLINUX}"
fi
if [ ! -d "${PERSISTLINUX}" ] ; then
	mkdir "${PERSISTLINUX}"
fi

while read -r loop; do
	if [[ $loop =~ p1 ]] ; then
		mount -o loop,rw "/dev/mapper/${loop}" "${BOOTLINUX}"
		echo "${BOOTLINUX} mounted from /dev/mapper/${loop}"
	fi
	if [[ $loop =~ p2 ]] ; then
        mount -o loop,rw "/dev/mapper/${loop}" "${PERSISTLINUX}"
		echo "${PERSISTLINUX} mounted from /dev/mapper/${loop}"
    fi
done <<< "$(kpartx -asv ${BOOTIMAGE} | cut -d' ' -f3)"
